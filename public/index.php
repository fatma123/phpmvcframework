<?php

/*
 * Php mvc framework
 *
 * @author fatma Abdelrahman
 *
 * fabdelrahman72@yahoo.com
 *   */


/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| this application. We just need to utilize it! We'll simply require it
| into the script here so we don't need to manually load our classes.
|
*/

require __DIR__ .'/../vendor/autoload.php';


/*
|--------------------------------------------------------------------------
| Bootstrap the  Application
|--------------------------------------------------------------------------
|
| Bootstrap the  Application and do action from framework.
|
*/

require __DIR__ .'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run the  Application
|--------------------------------------------------------------------------
|
| Handle the request and send response
|
*/
Application::run();

