<?php

//echo "hello from bootstrap";
use PhpMvcFrameWork\Bootstrap\App;
class Application
{
    /*
     * Application Constructor
     * */

    private function __construct()
    {

    }

    public  static function run(){
        /*
         * Define route path
         * */
        define('ROOT',realpath(__DIR__.'/..'));

        /*
        * Define directory separator
        * */
        define('DS',DIRECTORY_SEPARATOR);

        App::run();
    }

}